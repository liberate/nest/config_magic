Gem::Specification.new do |spec|
  spec.name        = "config_magic"
  spec.version     = "0.1.0"
  spec.authors     = ["liberate"]
  spec.homepage    = "https://0xacab.org/liberate/nest/config_magic"
  spec.summary     = "Configuration toolkit for Ruby on Rails"
  spec.license     = "MIT"
  spec.files = Dir["lib/**/*", "README.md"]
end
